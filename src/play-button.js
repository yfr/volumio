import React from "react";

import "./play-button.css";

/**
 * Returns the player interface component.
 * @param {Object} props The properties.
 * @return {JSX.Element}
 */
export default function PlayerButton({ status, onClick }) {
  return (
    <div onClick={onClick} className="playbuttonContainer">
      {renderIcon(status)}
    </div>
  );
}

function renderIcon(status) {
    console.log(status);
    
  if (/play/.test(status)) {
    return (
      <svg className="icon pause" viewBox="0 0 20 20">
        <path d="M5,5 L5,15 L15,15 L15,5 L5,5" fill="#000" />
      </svg>
    );
  }
  return (
    <svg className="icon play" viewBox="0 0 20 20">
      <path d="M5,0 L15,10 L5,20 L5,0" fill="#000" />
    </svg>
  );
}
