import React, { useState } from "react";
import "./App.css";
import PlayButton from "./play-button";
import useSocket from "use-socket.io-client";
function App() {
  const [playState, setPlayState] = useState({});
  const [radioList, setRadioList] = useState({});
  const [socket] = useSocket("http://192.168.178.26");

  socket.on("pushState", setPlayState);
  socket.on("pushBrowseLibrary", setRadioList);

  socket.on("connect", () => {
    socket.emit("getState");
    socket.emit("browseLibrary", { uri: "radio/myWebRadio" });
  });

  return (
    <div className="App">
      <header className="App-header">
        <button onClick={() => socket.emit("volume", "-")}>-</button>
        <h1>{playState.title}</h1>
        <button onClick={() => socket.emit("volume", "+")}>+</button>
      </header>
      <PlayButton
        status={playState.status}
        onClick={() => {
          socket.emit(playState.status === "play" ? "stop" : "play");
          socket.emit("getState");
        }}
      />
      {radioList.navigation &&
        radioList.navigation.lists[0].items.map(station => (
          <div
            key={station.uri}
            onClick={() =>
              socket.emit("addPlay", {
                service: station.service,
                title: station.title,
                uri: station.uri
              })
            }
          >
            {station.title}
          </div>
        ))}
    </div>
  );
}

export default App;
